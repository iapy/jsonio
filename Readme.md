
# Abstract

The `jsonio` library is a tool that enables JSON serialisation and deserialisation of `boost::hana` structures, utilising the [Nlohmann JSON](https://github.com/nlohmann/json)  framework:

```c++
#include <boost/hana.hpp>

#include <jsonio/deserialize.hpp>
#include <jsonio/serialize.hpp>

struct Test {
	BOOST_HANA_DEFINE_STRUCT(Test,
		(std::string, a),
		(std::vector<int>, b)
	);
};

Test foo{
	.a = "foo"s,
	.b = {1, 2, 3}
}, bar;

nlohmann::json j, k;
jsonio::to(j, foo);
k = jsonio::to(foo);

jsonio::from(k, bar);
bar = jsonio::from<Test>(k);
```

In addition to standard `JSON`-serialisable types like `std::string`, `std::vector`, `std::unordered_map<std::string, T>` and primitives, serialisation of `std::optional`, `std::variant`, and `std::duration` is also supported.

## Builtin serialisation

### `std::chrono::`

The serialised `std::chrono::duration` is represented as a string, which consist of a duration count and a subsequent duration suffix. For instance:

| c++                             | JSON |
| ------------------------------- | ---- |
| `std::chrono::nanoseconds(5)`   | 15ns |
| `std::chrono::microseconds(10)` | 10us |
| `std::chrono::milliseconds(15)` | 15ms |
| `std::crhono::seconds(20)`      | 20s  |
| `std::chrono::minutes(25)`      | 25m  |
| `std::chrono::hours(30)`        | 30h  |

When deserialised the duration suffix is considered and is used to construct suitable `std::chrono::duration_cast`:


| JSON | Target c++ type            | Target c++ value                 |
| ---- | -------------------------- | -------------------------------- |
| 15ns | `std::chrono::nanoseconds` | `std::chrono::nanoseconds(15)`   |
| 10us | `std::chrono::nanoseconds` | `std::chrono::nanosecods(10000)` |

### `std::variant`

The `std::variant` is serialised into a `JSON` dictionary, which contains a single key that represents the held type, and a value that corresponds to the variant’s value.


| C++                                         | Possible JSON              |
| ------------------------------------------- | -------------------------- |
| `std::variant<int, std::string>`            | `{"int": 42}`              |
|                                             | `{"string": "foo"}`        |
| `std::variant<std::vector<int>, float>`     | `{"vector<int>": [1,2,3]}` |
|                                             | `{"float": 3.14}`          |
| `std::variant<std::monostate, foo::Struct>` | `{"foo::Struct": {}}`      |
|                                             | `null`                     |

The keys in a target dictionary are determined as follows:

- If the type is primitive, the key is the literal name.
- If the type is `std::string`, the key is `string`.
- If the type is `std::vector`, the key is `vector<T>`, where T is constructed using the same rules.
- If the type is `std::monostate`, the key is `null`.
- For other types, the key is the demangled type name.

You can change the key for a type by using the following method:
```c++
template<>
struct jsonio::variant_key<foo::Struct>
{
	static std::string data()
	{
		return "foo::Struct";
	}
};
```

## Developer-defined serialisation

The `jsonio` library allows for the definition of serialisers for custom types:

```c++
struct Binary
{
	std::vector<char> data;
};

template<>
struct jsonio::Serializer<Binary>
{
	template<typename Data = void*>
	static void save(nlohmann::json &json,
		Binary const &binary, Data d = nullptr)
	{
		json = std::tmpnam(nullptr);

		std::ofstream(json.get<std::string>(), std::ios::bin).write(
			binary.data.c_str(), binary.data.size()
		);
	}

	template<typename Data = void*>
	static void load(nlohmann::json const &json,
		Binary &binary, Data d = nullptr)
	{
		auto const file = json.get<std::string>();

		binary.data.resize(std::filesystem::file_size(file));
		std::ifstream(file, std::ios::bin).read(
			binary.data.c_str(), binary.data.size()
		);
	}
};

Binary d {
	.data = get_data()
};
auto j = jsonio::to(d);
auto e = jsonio::from<Binary>(j);
```

In the provided code, we have defined a serialiser/deserialiser for a custom type `Binary` that contains binary data. When an instance of `Binary` is serialised, the binary data is stored in a temporary file, and the `json`value is set to this file path. During deserialisation, the binary data is read from this file.

Types for which custom serialisers are defined can be utilised in several ways: they can be members of a struct, elements of a vector or map, or even wrapped with `std::optional`:

```c++
struct Album {
	BOOST_HANA_DEFINE_STRUCT(Album,
		(std::string, name),
		(Binary, cover),
		(std::vector<Binary>, photos),
		(std::optional<Binary>, largeCover)
	);
};
```

You might have observed that there’s a third `Data` parameter in the `save` and `load` methods of the custom serialiser. This allows for the providing of any extra data to the serialisation process:

```c++
template<>
struct jsonio::Serializer<Binary>
{
	static void save(nlohmann::json &json,
		Binary const &binary, std::filesystem::path const *dir)
	{
		json = boost::uuids::to_string(
			boost::uuids::uuid());

		std::ofstream(
			(*dir) / json.get<std::string>(),
			std::ios::bin
		).write(binary.data.c_str(), binary.data.size());
	}

	static void load(nlohmann::json const &json,
		Binary &binary, std::filesystem::path const *dir)
	{
		auto const file = (*dir) / json.get<std::string>();
		binary.data.resize(std::filesystem::file_size(file));

		std::ifstream(file,std::ios::bin).read(
			binary.data.c_str(), binary.data.size()
		);
	}
};

Binary d {
	.data = get_data()
};

std::filesystem::path dir{"/opt/data"};

nlohmann::json j;
jsonio::to(d, j, &dir);
jsonio::from(d, j, &dir);
```

In the aforementioned example, binary data is stored in a file located at `/opt/data/{UUID}`, where `{UUID}`represents a generated UUID. Note that overrides for `jsonio::to` and `jsonio::from` that take a single argument are not accessible.

## JSON_STRUCT macro

The `JSON_STRUCT` macro can be used to serialise and deserialise intricate nested structures, which is particularly beneficial when creating a client for a web server. Let's consider an example for **Apple Music** search [API](https://developer.apple.com/library/archive/documentation/AudioVideo/Conceptual/iTuneSearchAPI/index.html):

```c++
JSON_STRUCT(Artists,
(
	JSON_STRUCT(Result,
	(
		(std::uint64_t, artistId),
		(std::string, artistName),
		(std::string, artistLinkUrl),
		(std::string, primaryGenreName),
		(std::uint64_t, primaryGenreId)
	));
),
(
	(std::uint64_t, resultCount),
	(std::vector<Result>, results)
));
```

The above defines the structure represents the results of the https://itunes.apple.com/search?media=music&entity=musicArtist&term=Artist+Name and could be used as follows (pseudo code):

```c++
auto response = client.get("https://itunes.apple.com/search?media=...");
Artists artists = jsonio::get<Artists>(response.body);
```

 * The `JSON_STRUCT` can contain an unlimited number of nested layers as needed.
 * The `JSON_STRUCT` wraps the `BOOST_BEAST_DEFINE_STRUCT`

When creating structures to manage complex service APIs, it could be beneficial to indicate the result of the service endpoint that this structure represents.

```c++
JSON_STRUCT(Artists, "/search?media=music&entity=musicArtist&term=",
(
	JSON_STRUCT(Result,
	(
		(std::uint64_t, artistId),
		(std::string, artistName),
		(std::string, artistLinkUrl),
		(std::string, primaryGenreName),
		(std::uint64_t, primaryGenreId)
	));
),
(
	(std::uint64_t, resultCount),
	(std::vector<Result>, results)
));

Artists search(std::string const &name)
{
	auto response = client.get(("https://itunes.apple.com"
		+ Artists::endpoint.data() + name)
	return jsonio::from<Artists>(response.body);
}
```

Certain web services might define `API`s where `JSON` includes C++ field names that are invalid as `JSON`keys (for example, [Dropbox](https://www.dropbox.com/developers/documentation/http/documentation#error-handling) wildly uses fields prefixed with dot). To address this issue, an additional macro, `JSON_RENAME_FIELDS`, is available and operates in the following manner:

```c++
JSON_STRUCT(Error,
(
	JSON_STRUCT(E,
	(
		JSON_RENAME_FIELDS((tag, .tag))
	),
	(
		(std::string, tag)
	))
)
(
	(std::string, error_summary),
	(E, error)
))
```

The `JSON_RENAME_FIELDS` can also be utilised with simple `boost::hana` structures (the subsequent code is the same as the preceding one):

```c++
struct Error
{
	struct E
	{
		BOOST_HANA_DEFINE_STRUCT(E,
			(std::string, tag)
		);
		JSON_RENAME_FIELDS((tag, .tag))
	};

	BOOST_HANA_DEFINE_STRUCT(Error,
		(std::string, error_summary),
		(E, error)
	);
};

auto j = jsonio::to(Error{});
```

In the code above, `j` would be equal to

```JSON
{
	"error": {".tag": ""},
	"error_summary": ""
}
```
