#pragma once

#include <ct/string.hpp>
#include <boost/hana.hpp>
#include <boost/core/demangle.hpp>

#include <nlohmann/json.hpp>
#include <unordered_map>
#include <unordered_set>
#include <type_traits>
#include <optional>
#include <variant>
#include <vector>
#include <map>
#include <set>

namespace jsonio::detail {
    
template<typename T>
auto is_natively_serializeable(T*) -> decltype(
    nlohmann::json{} = std::declval<T>(), std::true_type{}
);

template<typename T>
std::false_type is_natively_serializeable(...);

template<typename T>
constexpr bool is_natively_serializeable_v = decltype(is_natively_serializeable<T>(0))::value;

template<typename T>
auto has_json_field_names(T *value) -> decltype(T::_json_field_names(boost::hana::string<>{}), std::true_type{});

template<typename T>
std::false_type has_json_field_names(...);

template<typename T>
constexpr bool has_json_field_names_v = decltype(has_json_field_names<T>(nullptr))::value;

template<typename T>
constexpr bool is_container = false;

template<typename T, typename A>
constexpr bool is_container<std::vector<T, A>> = true;

template<typename T, typename A>
constexpr bool is_container<std::set<T, A>> = true;

template<typename T, typename A>
constexpr bool is_container<std::map<std::string, T, A>> = true;

template<typename T, typename A>
constexpr bool is_container<std::unordered_set<T, A>> = true;

template<typename T, typename A>
constexpr bool is_container<std::unordered_map<std::string, T, A>> = true;

template<typename T>
constexpr bool is_optional = false;

template<typename T>
constexpr bool is_optional<std::optional<T>> = true;

template<typename T>
constexpr std::monostate variant_key;

template<typename T>
constexpr bool is_specified = !std::is_same_v<std::decay_t<decltype(variant_key<T>)>, std::monostate>;

using namespace ct::string_literals;

template<>
constexpr auto variant_key<int> = "int"_cts;

template<>
constexpr auto variant_key<double> = "double"_cts;

template<>
constexpr auto variant_key<std::string> = "string"_cts;

template<typename T, typename Enable = void>
struct variant_key_t;

template<typename T>
struct variant_key_t<T, std::enable_if_t<
    (!is_specified<T> && !is_container<T>)
>>
{
    static std::string get()
    {
        return boost::core::demangle(typeid(T).name());
    }
};

template<typename T>
struct variant_key_t<T, std::enable_if_t<is_specified<T>>>
{
    static constexpr auto get()
    {
        return variant_key<T>;
    }
};

template<typename T, typename D, bool B = is_specified<T>>
struct variant_wrap_container
{
    static constexpr auto get()
    {
        return D::prefix + variant_key_t<T>::get();
    }
};

template<typename T, typename D>
struct variant_wrap_container<T, D, false>
{
    static std::string get()
    {
        return D::prefix + variant_key_t<T>::get();
    }
};

template<typename T, typename A>
struct variant_key_t<std::vector<T, A>>
: variant_wrap_container<T, variant_key_t<std::vector<T, A>>>
{
    static constexpr auto prefix = "vector."_cts;
};

template<typename T, typename A>
struct variant_key_t<std::set<T, A>>
: variant_wrap_container<T, variant_key_t<std::set<T, A>>>
{
    static constexpr auto prefix = "set."_cts;
};

template<typename T, typename A>
struct variant_key_t<std::map<std::string, T, A>>
: variant_wrap_container<T, variant_key_t<std::map<std::string, T, A>>>
{
    static constexpr auto prefix = "map."_cts;
};

template<typename T, typename A>
struct variant_key_t<std::unordered_set<T, A>>
: variant_wrap_container<T, variant_key_t<std::unordered_set<T, A>>>
{
    static constexpr auto prefix = "uset."_cts;
};

template<typename T, typename A>
struct variant_key_t<std::unordered_map<std::string, T, A>>
: variant_wrap_container<T, variant_key_t<std::unordered_map<std::string, T, A>>>
{
    static constexpr auto prefix = "umap."_cts;
};

} // namespace jsonio::detail

