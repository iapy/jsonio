#pragma once

#include <jsonio/struct.hpp>
#include <jsonio/traits.hpp>

#include <unordered_set>
#include <unordered_map>
#include <optional>
#include <sstream>
#include <variant>
#include <chrono>
#include <thread>

namespace jsonio {

template<typename T, typename Enabler = void>
struct Serializer;

template<typename T>
struct Serializer<T, std::enable_if_t<detail::is_natively_serializeable_v<T> && !std::is_enum_v<T> && !detail::is_optional<T>>>
{
    template<typename Data = void*>
    static void save(nlohmann::json &json, T const &value, Data d = nullptr)
    {
        json = value;
    }

    template<typename Data = void*>
    static void load(nlohmann::json const &json, T &value, Data d = nullptr)
    {
        value = json.get<T>();
    }
};

template<typename T>
struct Serializer<T, std::enable_if_t<std::is_enum_v<T>>>
{
    template<typename Data = void*>
    static void save(nlohmann::json &json, T const &value, Data d = nullptr)
    {
        if constexpr (std::is_same_v<std::underlying_type_t<T>, char>)
        {
            const char ch = static_cast<char>(value);
            json = std::string{&ch, 1};
        }
        else
        {
            json = static_cast<std::underlying_type_t<T>>(value);
        }
    }

    template<typename Data = void*>
    static void load(nlohmann::json const &json, T &value, Data d = nullptr)
    {
        if constexpr (std::is_same_v<std::underlying_type_t<T>, char>)
        {
            value = static_cast<T>(json.get<std::string>()[0]);
        }
        else
        {
            value = static_cast<T>(json.get<std::underlying_type_t<T>>(value));
        }
    }
};

template<typename T>
struct Serializer<T, std::enable_if_t<boost::hana::Struct<T>::value>>
{
    template<typename Data = void*>
    static void save(nlohmann::json &json, T const &value, Data d = nullptr)
    {
        using namespace boost::hana;
        for_each(accessors<T>(), [&json, &value, &d](auto &&accessor) {
            if constexpr (detail::has_json_field_names_v<T>)
            {
                auto constexpr name = T::template _json_field_names<std::remove_reference_t<decltype(first(accessor))>>().data();
                Serializer<std::decay_t<decltype(second(accessor)(value))>>::save(json[name], second(accessor)(value), d);
            }
            else
            {
                Serializer<std::decay_t<decltype(second(accessor)(value))>>::save(json[first(accessor).c_str()], second(accessor)(value), d);
            }
        });
    }

    template<typename Data = void*>
    static void load(nlohmann::json const &json, T &value, Data d = nullptr)
    {
        using namespace boost::hana;
        for_each(accessors<T>(), [&json, &value, &d](auto &&accessor){
            if constexpr (detail::has_json_field_names_v<T>)
            {
                auto constexpr name = T::template _json_field_names<std::remove_reference_t<decltype(first(accessor))>>().data();
                if(json.contains(name))
                    Serializer<std::decay_t<decltype(second(accessor)(value))>>::load(json[name], second(accessor)(value), d);
            }
            else
            {
                if(json.contains(first(accessor).c_str()))
                    Serializer<std::decay_t<decltype(second(accessor)(value))>>::load(json[first(accessor).c_str()], second(accessor)(value), d);
            }
        });
    }
};

template<typename V>
struct Serializer<std::unordered_map<std::string, V>, std::enable_if_t<!detail::is_natively_serializeable_v<V>>>
{
    template<typename Data = void*>
    static void save(nlohmann::json &json, std::unordered_map<std::string, V> const &value, Data d = nullptr)
    {
        for(auto const &[k, v] : value)
        {
            Serializer<V>::save(json[k], v, d);
        }
    }

    template<typename Data = void*>
    static void load(nlohmann::json const &json, std::unordered_map<std::string, V> &value, Data d = nullptr)
    {
        for(nlohmann::json::const_iterator it = json.begin(); it != json.end(); ++it)
        {
            if constexpr (std::is_copy_constructible_v<V>)
            {
                Serializer<V>::load(it.value(), value[it.key()], d);
            }
            else
            {
                V v;
                Serializer<V>::load(it.value(), v, d);
                value.emplace(it.key(), std::move(v));
            }
        }
    }
};

template<typename T>
struct Serializer<std::vector<T>, std::enable_if_t<!detail::is_natively_serializeable_v<T>>>
{
    template<typename Data = void*>
    static void save(nlohmann::json &json, std::vector<T> const &value, Data d = nullptr)
    {
        json = nlohmann::json::array();
        for(auto const &v : value)
        {
            json.push_back(nlohmann::json{});
            Serializer<T>::save(json.back(), v, d);
        }
    }

    template<typename Data = void*>
    static void load(nlohmann::json const &json, std::vector<T> &value, Data d = nullptr)
    {
        value.clear();
        for(auto const &o : json)
        {
            if constexpr (std::is_copy_constructible_v<T>)
            {
                value.emplace_back();
                Serializer<T>::load(o, value.back(), d);
            }
            else
            {
                T v;
                Serializer<T>::load(o, v, d);
                value.emplace_back(std::move(v));
            }
        }
    }
};

template<typename T>
struct Serializer<std::optional<T>>
{
    template<typename Data = void*>
    static void save(nlohmann::json &json, std::optional<T> const &value, Data d = nullptr)
    {
        if(value) Serializer<T>::save(json, *value, d);
        else json = nullptr;
    }

    template<typename Data = void*>
    static void load(nlohmann::json const &json, std::optional<T> &value, Data d = nullptr)
    {
        if(json.is_null()) value = std::nullopt;
        else Serializer<T>::load(json, value.emplace(), d);
    }
};

template<typename ...T>
struct Serializer<std::variant<T...>>
{
    template<typename Data = void*>
    static void save(nlohmann::json &json, std::variant<T...> const &value, Data d = nullptr)
    {
        save_<Data, T...>(json, value, d);
    }

    template<typename Data = void*>
    static void load(nlohmann::json const &json, std::variant<T...> &value, Data d = nullptr)
    {
        load_<Data, T...>(json, value, d);
    }
private:
    template<typename Data, typename Head, typename ...Tail>
    static void save_(nlohmann::json &json, std::variant<T...> const &value, Data d = nullptr)
    {
        if(std::holds_alternative<Head>(value))
        {
            if constexpr (std::is_same_v<Head, std::monostate>)
            {
                json = nullptr;
            }
            else
            {
                json.clear();
                Serializer<Head>::save(json[detail::variant_key_t<Head>::get()], std::get<Head>(value), d);
            }
        }
        else if constexpr (sizeof ...(Tail) != 0)
        {
            save_<Data, Tail...>(json, value, d);
        }
    }

    template<typename Data, typename Head, typename ...Tail>
    static void load_(nlohmann::json const &json, std::variant<T...> &value, Data d = nullptr)
    {
        if constexpr (std::is_same_v<Head, std::monostate>)
        {
            if(json.is_null())
            {
                value = std::monostate{};
            }
            else if constexpr (sizeof ...(Tail) != 0)
            {
                load_<Data, Tail...>(json, value, d);
            }
        }
        else
        {
            auto const name = detail::variant_key_t<Head>::get();
            if(json.contains(name))
            {
                Serializer<Head>::load(json[name], value.template emplace<Head>(), d);
            }
            else if constexpr (sizeof ...(Tail) != 0)
            {
                load_<Data, Tail...>(json, value, d);
            }
        }
    }
};

template<>
struct Serializer<std::thread::id>
{
    template<typename Data = void*>
    static void save(nlohmann::json &json, std::thread::id const &value, Data d = nullptr)
    {
        std::ostringstream ss;
        ss << value;
        json = ss.str();
    }
};

template<class Rep, class Period>
struct Serializer<std::chrono::duration<Rep, Period>>
{
    template<typename Data = void*>
    static void save(nlohmann::json &json, std::chrono::duration<Rep, Period> const &value, Data d = nullptr)
    {
        std::ostringstream ss;
        if constexpr (std::is_same_v<Period, std::nano>)
        {
            ss << value.count() << "ns";
        }
        else if constexpr (std::is_same_v<Period, std::micro>)
        {
            ss << value.count() << "us";
        }
        else if constexpr (std::is_same_v<Period, std::milli>)
        {
            ss << value.count() << "ms";
        }
        else if constexpr (std::is_same_v<Period, std::ratio<1>>)
        {
            ss << value.count() << "s";
        }
        else if constexpr (std::is_same_v<Period, std::ratio<60>>)
        {
            ss << value.count() << "m";
        }
        else if constexpr (std::is_same_v<Period, std::ratio<3600>>)
        {
            ss << value.count() << "h";
        }
        else
        {
            ss << std::chrono::duration_cast<std::chrono::seconds>(value).count();
        }
        json = ss.str();
    }

    template<typename Data = void*>
    static void load(nlohmann::json const &json, std::chrono::duration<Rep, Period> &value, Data d = nullptr)
    {
        auto const &str = json.get<std::string>();

        std::int64_t repr;
        std::string suffix;
        std::istringstream{str.c_str()} >> repr >> suffix;

        if(suffix == "ns")
        {
            value = std::chrono::duration_cast<std::chrono::duration<Rep, Period>>(std::chrono::nanoseconds{repr});
        }
        else if(suffix == "us")
        {
            value = std::chrono::duration_cast<std::chrono::duration<Rep, Period>>(std::chrono::microseconds{repr});
        }
        else if(suffix == "ms")
        {
            value = std::chrono::duration_cast<std::chrono::duration<Rep, Period>>(std::chrono::milliseconds{repr});
        }
        else if(suffix == "s")
        {
            value = std::chrono::duration_cast<std::chrono::duration<Rep, Period>>(std::chrono::seconds{repr});
        }
        else if(suffix == "m")
        {
            value = std::chrono::duration_cast<std::chrono::duration<Rep, Period>>(std::chrono::minutes{repr});
        }
        else if(suffix == "h")
        {
            value = std::chrono::duration_cast<std::chrono::duration<Rep, Period>>(std::chrono::hours{repr});
        }
        else
        {
            value = std::chrono::duration_cast<std::chrono::duration<Rep, Period>>(std::chrono::seconds{repr});
        }
    }
};

} // namespace jsonio

