#pragma once
#include <jsonio/serializer.hpp>

namespace jsonio {

template<typename T, typename Data = void>
void to(nlohmann::json &json, T const &value, Data* data = nullptr)
{
    Serializer<T>::save(json, value, data);
}

template<typename T>
nlohmann::json to(T const &value)
{
    nlohmann::json json;
    to<T, void>(json, value, nullptr);
    return json;
}

}
