#pragma once
#include <jsonio/serializer.hpp>

namespace jsonio {

template<typename T, typename Data = void*>
void from(nlohmann::json const &json, T &value, Data data = nullptr)
{
    Serializer<T>::load(json, value, data);
}

template<typename T>
T from(nlohmann::json const &json)
{
    T value;
    from(json, value);
    return std::move(value);
}

}
