#pragma once
#include <boost/preprocessor.hpp>
#include <boost/hana.hpp>

#include <unordered_map>
#include <string>

namespace jsonio
{
    template<typename T>
    using map = std::unordered_map<std::string, T>;
}

#define JSON_DETAIL_RENAME_FIELD_(x, y) \
    boost::hana::make_pair(BOOST_HANA_STRING(#x), BOOST_HANA_STRING(#y))

#define JSON_DETAIL_RENAME_FIELD(r, data, i, elem) \
    BOOST_PP_COMMA_IF(i) JSON_DETAIL_RENAME_FIELD_ elem

#define JSON_RENAME_FIELDS(...) \
    template<typename T> static constexpr std::string_view _json_field_names(T v = T{}) {               \
        constexpr auto MAPPING = boost::hana::make_map(                                                 \
            BOOST_PP_SEQ_FOR_EACH_I(JSON_DETAIL_RENAME_FIELD, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__)) \
        );                                                                                              \
        if constexpr (boost::hana::contains(MAPPING, v)) {                                              \
            return {MAPPING[v].c_str(), boost::hana::size(MAPPING[v])};                                 \
        }                                                                                               \
        return {v.c_str(), boost::hana::size(v)};                                                       \
    }

#define JSON_STRUCT_DETAIL_UNPACK(...) __VA_ARGS__

#define JSON_STRUCT_2(name, fields) \
    struct name { BOOST_HANA_DEFINE_STRUCT(name, JSON_STRUCT_DETAIL_UNPACK fields); }

#define JSON_STRUCT_3(name, classes, fields) \
    struct name { JSON_STRUCT_DETAIL_UNPACK classes BOOST_HANA_DEFINE_STRUCT(name, JSON_STRUCT_DETAIL_UNPACK fields); }

#define JSON_STRUCT_4(name, e, classes, fields) \
    struct name { static constexpr std::string_view endpoint{e}; BOOST_PP_SEQ_ENUM(classes) BOOST_HANA_DEFINE_STRUCT(name, JSON_STRUCT_DETAIL_UNPACK fields); }

#define JSON_STRUCT(...) \
    BOOST_PP_OVERLOAD(JSON_STRUCT_, __VA_ARGS__)(__VA_ARGS__)

