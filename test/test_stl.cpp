#include <jsonio/serialize.hpp>
#include <jsonio/deserialize.hpp>
#include <tester/tester.hpp>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <unordered_map>
#include <unordered_set>

BOOST_AUTO_TEST_SUITE(Stl)

struct Test
{
    BOOST_HANA_DEFINE_STRUCT(Test,
        (std::unordered_map<std::string, int>, a),
        (std::unordered_set<std::string>, b),
        (std::unordered_map<std::string, nlohmann::json>, c)
    );
};

std::ostream &operator << (std::ostream &stream, Test const &test)
{
    return stream << jsonio::to(test).dump();
}

BOOST_AUTO_TEST_CASE(TestIO)
{
    using namespace std::string_literals;

    Test const origin{
        .a = {
            {"foo", 1},
            {"bar", 2}
        },
        .b = {"x"s, "y"s, "z"s},
        .c = {
            {"a", "{\"x\": 1, \"y\": 2}"_json},
            {"b", "[1,2,3,4,5]"_json}
        }
    };

    nlohmann::json j;
    jsonio::to(j, origin);
    Test test;
    jsonio::from(j, test);

    BOOST_TEST(origin.a == test.a);
    BOOST_TEST(origin.b == test.b);
}

BOOST_AUTO_TEST_SUITE_END()
