#include <jsonio/serialize.hpp>
#include <jsonio/deserialize.hpp>
#include <jsonio/struct.hpp>
#include <tester/tester.hpp>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <vector>
#include <string>

struct CustomTypes
{
    struct Test
    {
        BOOST_HANA_DEFINE_STRUCT(Test,
            (std::string, a),
            (std::vector<int>, b),
            (Test const *, c)
        );
    };

    struct Storage
    {
        boost::uuids::uuid id;
        Test const *ptr;
    };
};

namespace jsonio {

template<>
struct Serializer<CustomTypes::Test const *>
{
    static void save(nlohmann::json &json, CustomTypes::Test const * const value, CustomTypes::Storage *storage)
    {
        if(value == nullptr) {
            json = nullptr;
        }
        else {
            storage->id = boost::uuids::uuid();
            json = boost::uuids::to_string(storage->id);
        }
    }

    static void load(nlohmann::json const &json, CustomTypes::Test const *&value, CustomTypes::Storage *storage)
    {
        if(json.is_null()) {
            value = nullptr;
        }
        else {
            BOOST_TEST(boost::uuids::to_string(storage->id) == json.get<std::string>());
            value = storage->ptr;
        }
    }
};

} // namespace jsonio

BOOST_FIXTURE_TEST_SUITE(Custom, CustomTypes)

BOOST_AUTO_TEST_CASE(TestIO)
{
    using namespace std::string_literals;

    Test const parent{
        .a = "foo"s,
        .b = {1, 2, 3},
        .c = nullptr
    };

    Test const origin{
        .a = "bar"s,
        .b = {4, 5, 6},
        .c = &parent
    };

    Storage storage;

    nlohmann::json pj, oj;
    jsonio::to(pj, parent, &storage);
    jsonio::to(oj, origin, &storage);

    Test parent_;
    jsonio::from(pj, parent_, &storage);

    BOOST_TEST(parent.a == parent_.a);
    BOOST_TEST(parent.b == parent_.b);
    BOOST_TEST(parent.c == parent_.c);

    storage.ptr = &parent_;
    
    Test origin_;
    jsonio::from(oj, origin_, &storage);

    BOOST_TEST(origin.a == origin_.a);
    BOOST_TEST(origin.b == origin_.b);
    BOOST_TEST(origin_.c == &parent_);
}

BOOST_AUTO_TEST_SUITE_END()
