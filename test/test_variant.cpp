#include <jsonio/serialize.hpp>
#include <jsonio/deserialize.hpp>
#include <tester/tester.hpp>

#include <vector>
#include <string>
#include <variant>

namespace std {
    template<typename ...A>
    ostream &operator << (ostream &stream, variant<A...> const &v)
    {
        return stream << jsonio::to(v).dump();
    }
}

BOOST_AUTO_TEST_SUITE(Variant)

BOOST_AUTO_TEST_CASE(Test_1)
{
    using namespace std::string_literals;
    nlohmann::json j;

    std::variant<int, std::string, std::vector<double>> origin, copy;

    j = jsonio::to(origin);
    jsonio::from(j, copy);
    BOOST_TEST(copy == origin);

    origin = "foobar";

    j = jsonio::to(origin);
    BOOST_TEST(j.contains("string"));
    jsonio::from(j, copy);
    BOOST_TEST(copy == origin);

    origin = 42;

    j = jsonio::to(origin);
    BOOST_TEST(j.contains("int"));
    jsonio::from(j, copy);
    BOOST_TEST(copy == origin);

    origin = std::vector{1.2, 3.4};

    j = jsonio::to(origin);
    BOOST_TEST(j.contains("vector.double"));
    jsonio::from(j, copy);
    BOOST_TEST(copy == origin);
}

BOOST_AUTO_TEST_CASE(Test_2)
{
    using namespace std::string_literals;
    nlohmann::json j;

    std::variant<std::monostate, int, std::string> origin, copy;

    j = jsonio::to(origin);
    jsonio::from(j, copy);
    BOOST_TEST(copy == origin);
    BOOST_TEST(j.dump() == "null");

    origin = "foobar";

    j = jsonio::to(origin);
    BOOST_TEST(j.contains("string"));
    jsonio::from(j, copy);
    BOOST_TEST(copy == origin);

    origin = 42;

    j = jsonio::to(origin);
    BOOST_TEST(j.contains("int"));
    jsonio::from(j, copy);
    BOOST_TEST(copy == origin);
}

BOOST_AUTO_TEST_SUITE_END()
