#include <jsonio/serialize.hpp>
#include <jsonio/deserialize.hpp>
#include <jsonio/struct.hpp>
#include <tester/tester.hpp>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <vector>
#include <string>

BOOST_AUTO_TEST_SUITE(Struct)

JSON_STRUCT(Test,
(
    JSON_STRUCT(A,
    (
        JSON_STRUCT(B,
        (
            (int, x),
            (std::string, y)
        ));
        JSON_STRUCT(C,
        (
            (int, x),
            (std::string, y)
        ));
    ),
    (
        (C, c),
        (std::string, name),
        (jsonio::map<B>, properties)
    ));
),
(
    (std::string, name),
    (std::vector<A>, values)
));

std::ostream &operator << (std::ostream &stream, Test const &test)
{
    return stream << jsonio::to(test).dump();
}

BOOST_AUTO_TEST_CASE(TestIO)
{
    using namespace std::string_literals;

    Test test; 
    jsonio::from(R"({
        "name": "test",
        "values": [
            {
                "c": { "x": 3, "y": "three" },
                "name": "value-1",
                "properties": {
                    "a": { "x": 1, "y": "one" },
                    "b": { "x": 2, "y": "two" }
                }
            },
            {
                "c": { "x": 5, "y": "funf" },
                "name": "value-2",
                "properties": {
                    "z": { "x": 9, "y": "neun" }
                }
            }
        ]
    })"_json, test);

    BOOST_TEST(test.name == "test");
    BOOST_TEST(test.values.size() == 2);
    BOOST_TEST(test.values[0].c.x == 3);
    BOOST_TEST(test.values[0].c.y == "three");
    BOOST_TEST(test.values[0].name == "value-1");
    BOOST_TEST(test.values[0].properties.size() == 2);
    BOOST_TEST(test.values[0].properties["a"].x == 1);
    BOOST_TEST(test.values[0].properties["a"].y == "one");
    BOOST_TEST(test.values[0].properties["b"].x == 2);
    BOOST_TEST(test.values[0].properties["b"].y == "two");

    BOOST_TEST(test.values[1].c.x == 5);
    BOOST_TEST(test.values[1].c.y == "funf");
    BOOST_TEST(test.values[1].name == "value-2");
    BOOST_TEST(test.values[1].properties.size() == 1);
    BOOST_TEST(test.values[1].properties["z"].x == 9);
    BOOST_TEST(test.values[1].properties["z"].y == "neun");
}

BOOST_AUTO_TEST_SUITE_END()
