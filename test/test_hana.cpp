#include <jsonio/serialize.hpp>
#include <jsonio/deserialize.hpp>
#include <tester/tester.hpp>
#include <vector>

BOOST_AUTO_TEST_SUITE(Hana)

struct Test
{
    BOOST_HANA_DEFINE_STRUCT(Test,
        (std::string, a),
        (std::vector<int>, b)
    );
};

std::ostream &operator << (std::ostream &stream, Test const &test)
{
    return stream << jsonio::to(test).dump();
}

BOOST_AUTO_TEST_CASE(TestIO)
{
    using namespace std::string_literals;

    nlohmann::json j;

    Test origin{
        .a = "foo"s,
        .b = {1, 2, 3}
    };

    jsonio::to(j, origin);

    Test test;
    jsonio::from(j, test);

    BOOST_TEST(origin.a == test.a);
    BOOST_TEST(origin.b == test.b);
}

BOOST_AUTO_TEST_SUITE_END()
