#include <jsonio/serialize.hpp>
#include <jsonio/deserialize.hpp>
#include <jsonio/struct.hpp>
#include <vector>

#include <jsonio/serialize.hpp>
#include <jsonio/deserialize.hpp>
#include <tester/tester.hpp>

BOOST_AUTO_TEST_SUITE(Rename)

struct Foo
{
    struct Fzz
    {
        BOOST_HANA_DEFINE_STRUCT(Fzz,
            (int, x)
        );
        JSON_RENAME_FIELDS((x, .x))
    };

    BOOST_HANA_DEFINE_STRUCT(Foo,
        (std::string, a),
        (std::vector<int>, b),
        (Fzz, c)
    );

    JSON_RENAME_FIELDS((a, .a));
};

JSON_STRUCT(Bar,
(
    JSON_STRUCT(Bzz,
    (
        JSON_RENAME_FIELDS((x, .x))
    ),
    (
        (int, x)
    ));
    JSON_RENAME_FIELDS((a, .a))
),
(
    (std::string, a),
    (std::vector<int>, b),
    (Bzz, c)
));

std::ostream &operator << (std::ostream &stream, Foo const &foo)
{
    return stream << jsonio::to(foo).dump();
}

std::ostream &operator << (std::ostream &stream, Bar const &bar)
{
    return stream << jsonio::to(bar).dump();
}

BOOST_AUTO_TEST_CASE(TestIO)
{
    using namespace std::string_literals;
    
    auto j = R"({
        ".a": "foo",
        "b": [1, 2, 3],
        "c": { ".x": 42 }
    })"_json;

    Foo foo;
    jsonio::from(j, foo);

    BOOST_TEST(foo.a == "foo");
    BOOST_TEST(foo.b.size() == 3);
    BOOST_TEST(foo.c.x == 42);

    Bar bar;
    jsonio::from(j, bar);
    
    BOOST_TEST(bar.a == "foo");
    BOOST_TEST(bar.b.size() == 3);
    BOOST_TEST(bar.c.x == 42);
}

BOOST_AUTO_TEST_SUITE_END()
