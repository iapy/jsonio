#include <jsonio/serialize.hpp>
#include <jsonio/deserialize.hpp>
#include <jsonio/struct.hpp>
#include <tester/tester.hpp>

#include <vector>
#include <string>

BOOST_AUTO_TEST_SUITE(Duration)

struct Test
{
    BOOST_HANA_DEFINE_STRUCT(Test,
        (std::chrono::nanoseconds, ns),
        (std::chrono::microseconds, us),
        (std::chrono::milliseconds, ms),
        (std::chrono::seconds, s),
        (std::chrono::minutes, m),
        (std::chrono::hours, h)
    );

    bool operator == (Test const &other) const
    {
        return ns == other.ns && ms == other.ms && us == other.us && s == other.s && m == other.m;
    }
};

std::ostream &operator << (std::ostream &stream, Test const &test)
{
    return stream << jsonio::to(test).dump();
}

BOOST_AUTO_TEST_CASE(TestIO)
{
    using namespace std::string_literals;
    nlohmann::json j;
    
    Test origin{
        .ns = std::chrono::nanoseconds{12},
        .us = std::chrono::microseconds{45},
        .ms = std::chrono::milliseconds{165},
        .s = std::chrono::seconds{20},
        .m = std::chrono::minutes{30},
        .h = std::chrono::hours{5}
    }, copy;

    j = jsonio::to(origin);
    jsonio::from(j, copy);
    BOOST_TEST(copy == origin);

    j["us"] = "45000ns";
    j["ms"] = "165000000ns";
    j["s"] = "20000ms";
    j["m"] = "1800s";
    j["h"] = "300m";

    jsonio::from(j, copy);
    BOOST_TEST(copy == origin);

    j = jsonio::to(copy);

    origin.ns = std::chrono::nanoseconds{12000};
    origin.us = std::chrono::microseconds{14000};
    origin.ms = std::chrono::milliseconds{16000};
    origin.h = std::chrono::hours{2};
    origin.s = std::chrono::hours{5};

    j["ns"] = "12us";
    j["us"] = "14ms";
    j["ms"] = "16s";
    j["h"] = "120m";
    j["s"] = "18000s";

    jsonio::from(j, copy);
    BOOST_TEST(copy == origin);
}

BOOST_AUTO_TEST_SUITE_END()
