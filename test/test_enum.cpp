#include <jsonio/serialize.hpp>
#include <jsonio/deserialize.hpp>
#include <tester/tester.hpp>

BOOST_AUTO_TEST_SUITE(Enum)

struct Test
{
    enum class Type : char
    {
        Foo = 'F',
        Bar = 'B'
    };

    BOOST_HANA_DEFINE_STRUCT(Test,
        (Type, a),
        (Type, b)
    );
};

std::ostream &operator << (std::ostream &stream, Test const &test)
{
    return stream << jsonio::to(test).dump();
}

std::ostream &operator << (std::ostream &stream, Test::Type const &type)
{
    return stream << static_cast<char>(type);
}

BOOST_AUTO_TEST_CASE(TestIO)
{
    using namespace std::string_literals;

    Test const origin {
        .a = Test::Type::Foo,
        .b = Test::Type::Bar
    };

    nlohmann::json j;
    jsonio::to(j, origin);

    BOOST_TEST(j["a"] == "F");
    BOOST_TEST(j["b"] == "B");

    Test test;
    jsonio::from(j, test);

    BOOST_TEST(test.a == Test::Type::Foo);
    BOOST_TEST(test.b == Test::Type::Bar);
}

BOOST_AUTO_TEST_SUITE_END()
